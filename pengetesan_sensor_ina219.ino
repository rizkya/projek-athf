#include <FuzzyRule.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>
float input1 = 0;
float input2 = 0;
float input3 = 0;
Fuzzy *fuzzy = new Fuzzy();

//Fuzzy Suhu Tubuh
FuzzySet *hipotermia = new FuzzySet(-1, 0, 27, 36);
FuzzySet *normal = new FuzzySet(27, 36, 37.5, 46.5);
FuzzySet *hipetermia = new FuzzySet(37.5, 46.5, 73.5, 74);

//Fuzzy Detak Jantung
FuzzySet *bradycardia = new FuzzySet(-1, 0, 45, 60);
FuzzySet *standard = new FuzzySet(45, 60, 100, 115);
FuzzySet *tachycardia = new FuzzySet(100, 115, 160, 161);

//Fuzzy Saturasi Oksigen
FuzzySet *less = new FuzzySet(-1, 0, 90, 91);
FuzzySet *belowavg = new FuzzySet(90, 91, 94, 95);
FuzzySet *average = new FuzzySet(94, 95, 100, 101);

//Fuzzy Kondisi Pasien
FuzzySet *sangat_tidak_optimal = new FuzzySet(-1, 0, 25, 37.5);
FuzzySet *tidak_optimal = new FuzzySet(25, 37.5, 62.5, 75);
FuzzySet *optimal = new FuzzySet(62.5, 75, 100, 101);

void fuzzy_begin()
{
  FuzzyInput *suhu = new FuzzyInput(1);
  suhu->addFuzzySet(hipotermia);
  suhu->addFuzzySet(normal);
  suhu->addFuzzySet(hipetermia);
  fuzzy->addFuzzyInput(suhu);

  FuzzyInput *bpm = new FuzzyInput(2);
  bpm->addFuzzySet(bradycardia);
  bpm->addFuzzySet(standard);
  bpm->addFuzzySet(tachycardia);
  fuzzy->addFuzzyInput(bpm);

  FuzzyInput *spo2 = new FuzzyInput(3);
  suhu->addFuzzySet(less);
  suhu->addFuzzySet(belowavg);
  suhu->addFuzzySet(average);
  fuzzy->addFuzzyInput(spo2);

  FuzzyOutput *kondisi = new FuzzyOutput(1);
  kondisi->addFuzzySet(optimal);
  kondisi->addFuzzySet(tidak_optimal);
  kondisi->addFuzzySet(sangat_tidak_optimal);
  fuzzy->addFuzzyOutput(kondisi);

  //Hipetermia Tachycardia
  FuzzyRuleAntecedent *hipetachy = new FuzzyRuleAntecedent();
  hipetachy->joinWithAND(hipetermia, tachycardia);

  //1
  FuzzyRuleAntecedent *hipetachyavg = new FuzzyRuleAntecedent();
  hipetachyavg->joinWithAND(hipetachy, average);

  //1
  FuzzyRuleConsequent *out1 = new FuzzyRuleConsequent();
  out1->addOutput(tidak_optimal);

  //rule 1
  FuzzyRule *FuzzyRule1 = new FuzzyRule(1, hipetachyavg, out1);
  fuzzy->addFuzzyRule(FuzzyRule1);

  //2
  FuzzyRuleAntecedent *hipetachybavg = new FuzzyRuleAntecedent();
  hipetachybavg->joinWithAND(hipetachy, belowavg);

  //2
  FuzzyRuleConsequent *out2 = new FuzzyRuleConsequent();
  out2->addOutput(sangat_tidak_optimal);

  //rule 2
  FuzzyRule *FuzzyRule2 = new FuzzyRule(2, hipetachybavg, out2);
  fuzzy->addFuzzyRule(FuzzyRule2);

  //3
  FuzzyRuleAntecedent *hipetachyless = new FuzzyRuleAntecedent();
  hipetachyless->joinWithAND(hipetachy, less);

  //3
  FuzzyRuleConsequent *out3 = new FuzzyRuleConsequent();
  out3->addOutput(sangat_tidak_optimal);

  //rule 3
  FuzzyRule *FuzzyRule3 = new FuzzyRule(3, hipetachyless, out3);
  fuzzy->addFuzzyRule(FuzzyRule3);

  //Hipetermia Standard
  FuzzyRuleAntecedent *hipestd = new FuzzyRuleAntecedent();
  hipestd->joinWithAND(hipetermia, standard);

  //4
  FuzzyRuleAntecedent *hipestdavg = new FuzzyRuleAntecedent();
  hipestdavg->joinWithAND(hipestd, average);

  //4
  FuzzyRuleConsequent *out4 = new FuzzyRuleConsequent();
  out4->addOutput(tidak_optimal);

  //rule 4
  FuzzyRule *FuzzyRule4 = new FuzzyRule(4, hipestdavg, out4);
  fuzzy->addFuzzyRule(FuzzyRule4);

  //5
  FuzzyRuleAntecedent *hipestdbavg = new FuzzyRuleAntecedent();
  hipestdbavg->joinWithAND(hipestd, belowavg);

  //5
  FuzzyRuleConsequent *out5 = new FuzzyRuleConsequent();
  out5->addOutput(tidak_optimal);

  //rule 5
  FuzzyRule *FuzzyRule5 = new FuzzyRule(5, hipestdbavg, out5);
  fuzzy->addFuzzyRule(FuzzyRule5);

  //6
  FuzzyRuleAntecedent *hipestdless = new FuzzyRuleAntecedent();
  hipestdless->joinWithAND(hipestd, less);

  //6
  FuzzyRuleConsequent *out6 = new FuzzyRuleConsequent();
  out6->addOutput(tidak_optimal);

  //rule 6
  FuzzyRule *FuzzyRule6 = new FuzzyRule(6, hipestdless, out6);
  fuzzy->addFuzzyRule(FuzzyRule6);

  //Hipetermia Bradycardia
  FuzzyRuleAntecedent *hipebrady = new FuzzyRuleAntecedent();
  hipebrady->joinWithAND(hipetermia, bradycardia);

  //7
  FuzzyRuleAntecedent *hipebradyavg = new FuzzyRuleAntecedent();
  hipebradyavg->joinWithAND(hipebrady, average);

  //7
  FuzzyRuleConsequent *out7 = new FuzzyRuleConsequent();
  out7->addOutput(tidak_optimal);

  //rule 7
  FuzzyRule *FuzzyRule7 = new FuzzyRule(7, hipebradyavg, out7);
  fuzzy->addFuzzyRule(FuzzyRule7);

  //8
  FuzzyRuleAntecedent *hipebradybavg = new FuzzyRuleAntecedent();
  hipebradybavg->joinWithAND(hipebrady, belowavg);

  //8
  FuzzyRuleConsequent *out8 = new FuzzyRuleConsequent();
  out8->addOutput(tidak_optimal);

  //rule 8
  FuzzyRule *FuzzyRule8 = new FuzzyRule(8, hipebradybavg, out8);
  fuzzy->addFuzzyRule(FuzzyRule8);

  //9
  FuzzyRuleAntecedent *hipebradyless = new FuzzyRuleAntecedent();
  hipebradyless->joinWithAND(hipebrady, less);

  //9
  FuzzyRuleConsequent *out9 = new FuzzyRuleConsequent();
  out9->addOutput(sangat_tidak_optimal);

  //rule 9
  FuzzyRule *FuzzyRule9 = new FuzzyRule(9, hipebradyless, out9);
  fuzzy->addFuzzyRule(FuzzyRule9);

  //Normal Tachycardia
  FuzzyRuleAntecedent *normtachy = new FuzzyRuleAntecedent();
  normtachy->joinWithAND(normal, tachycardia);

  //10
  FuzzyRuleAntecedent *normtachyavg = new FuzzyRuleAntecedent();
  normtachyavg->joinWithAND(normtachy, average);

  //10
  FuzzyRuleConsequent *out10 = new FuzzyRuleConsequent();
  out10->addOutput(tidak_optimal);

  //rule 10
  FuzzyRule *FuzzyRule10 = new FuzzyRule(10, normtachyavg, out10);
  fuzzy->addFuzzyRule(FuzzyRule10);

  //11
  FuzzyRuleAntecedent *normtachybavg = new FuzzyRuleAntecedent();
  normtachybavg->joinWithAND(normtachy, belowavg);

  //11
  FuzzyRuleConsequent *out11 = new FuzzyRuleConsequent();
  out11->addOutput(tidak_optimal);

  //rule 11
  FuzzyRule *FuzzyRule11 = new FuzzyRule(11, normtachybavg, out11);
  fuzzy->addFuzzyRule(FuzzyRule11);

  //12
  FuzzyRuleAntecedent *normtachyless = new FuzzyRuleAntecedent();
  normtachyless->joinWithAND(normtachy, less);

  //12
  FuzzyRuleConsequent *out12 = new FuzzyRuleConsequent();
  out12->addOutput(tidak_optimal);

  //rule 12
  FuzzyRule *FuzzyRule12 = new FuzzyRule(12, normtachyless, out12);
  fuzzy->addFuzzyRule(FuzzyRule12);

  //Normal Standard
  FuzzyRuleAntecedent *normstd = new FuzzyRuleAntecedent();
  normstd->joinWithAND(normal, standard);

  //13
  FuzzyRuleAntecedent *normstdavg = new FuzzyRuleAntecedent();
  normstdavg->joinWithAND(normstd, average);

  //13
  FuzzyRuleConsequent *out13 = new FuzzyRuleConsequent();
  out13->addOutput(optimal);

  //rule 13
  FuzzyRule *FuzzyRule13 = new FuzzyRule(13, normstdavg, out13);
  fuzzy->addFuzzyRule(FuzzyRule13);

  //14
  FuzzyRuleAntecedent *normstdbavg = new FuzzyRuleAntecedent();
  normstdbavg->joinWithAND(normstd, belowavg);

  //14
  FuzzyRuleConsequent *out14 = new FuzzyRuleConsequent();
  out14->addOutput(tidak_optimal);

  //rule 14
  FuzzyRule *FuzzyRule14 = new FuzzyRule(14, normstdbavg, out14);
  fuzzy->addFuzzyRule(FuzzyRule14);

  //15
  FuzzyRuleAntecedent *normstdless = new FuzzyRuleAntecedent();
  normstdless->joinWithAND(normstd, less);

  //15
  FuzzyRuleConsequent *out15 = new FuzzyRuleConsequent();
  out15->addOutput(tidak_optimal);

  //rule 15
  FuzzyRule *FuzzyRule15 = new FuzzyRule(15, normstdless, out15);
  fuzzy->addFuzzyRule(FuzzyRule15);

  //Normal Bradycardia
  FuzzyRuleAntecedent *normbrady = new FuzzyRuleAntecedent();
  normbrady->joinWithAND(normal, bradycardia);

  //16
  FuzzyRuleAntecedent *normbradyavg = new FuzzyRuleAntecedent();
  normbradyavg->joinWithAND(normbrady, average);

  //16
  FuzzyRuleConsequent *out16 = new FuzzyRuleConsequent();
  out16->addOutput(tidak_optimal);

  //rule 16
  FuzzyRule *FuzzyRule16 = new FuzzyRule(16, normstdless, out16);
  fuzzy->addFuzzyRule(FuzzyRule16);

  //17
  FuzzyRuleAntecedent *normbradybavg = new FuzzyRuleAntecedent();
  normbradybavg->joinWithAND(normbrady, belowavg);

  //17
  FuzzyRuleConsequent *out17 = new FuzzyRuleConsequent();
  out17->addOutput(tidak_optimal);

  //rule 17
  FuzzyRule *FuzzyRule17 = new FuzzyRule(17, normbradybavg, out17);
  fuzzy->addFuzzyRule(FuzzyRule17);

  //18
  FuzzyRuleAntecedent *normbradyless = new FuzzyRuleAntecedent();
  normbradyless->joinWithAND(normbrady, less);

  //18
  FuzzyRuleConsequent *out18 = new FuzzyRuleConsequent();
  out18->addOutput(tidak_optimal);

  //rule 18
  FuzzyRule *FuzzyRule18 = new FuzzyRule(18, normbradyless, out18);
  fuzzy->addFuzzyRule(FuzzyRule18);

  //Hipotermia Tachycardia
  FuzzyRuleAntecedent *hipotachy = new FuzzyRuleAntecedent();
  hipotachy->joinWithAND(hipotermia, tachycardia);

  //19
  FuzzyRuleAntecedent *hipotachyavg = new FuzzyRuleAntecedent();
  hipotachyavg->joinWithAND(hipotachy, average);

  //19
  FuzzyRuleConsequent *out19 = new FuzzyRuleConsequent();
  out19->addOutput(tidak_optimal);

  //rule 19
  FuzzyRule *FuzzyRule19 = new FuzzyRule(19, hipotachyavg, out19);
  fuzzy->addFuzzyRule(FuzzyRule19);

  //20
  FuzzyRuleAntecedent *hipotachybavg = new FuzzyRuleAntecedent();
  hipotachybavg->joinWithAND(hipotachy, belowavg);

  //20
  FuzzyRuleConsequent *out20 = new FuzzyRuleConsequent();
  out20->addOutput(tidak_optimal);

  //rule 20
  FuzzyRule *FuzzyRule20 = new FuzzyRule(20, hipotachybavg, out20);
  fuzzy->addFuzzyRule(FuzzyRule20);

  //21
  FuzzyRuleAntecedent *hipotachyless = new FuzzyRuleAntecedent();
  hipotachyless->joinWithAND(hipotachy, less);

  //21
  FuzzyRuleConsequent *out21 = new FuzzyRuleConsequent();
  out21->addOutput(sangat_tidak_optimal);

  //rule 21
  FuzzyRule *FuzzyRule21 = new FuzzyRule(21, hipotachyless, out21);
  fuzzy->addFuzzyRule(FuzzyRule21);

  //Hipotermia Standard
  FuzzyRuleAntecedent *hipostd = new FuzzyRuleAntecedent();
  hipostd->joinWithAND(hipotermia, standard);

  //22
  FuzzyRuleAntecedent *hipostdavg = new FuzzyRuleAntecedent();
  hipostdavg->joinWithAND(hipostd, average);

  //22
  FuzzyRuleConsequent *out22 = new FuzzyRuleConsequent();
  out22->addOutput(tidak_optimal);

  //rule 22
  FuzzyRule *FuzzyRule22 = new FuzzyRule(22, hipostdavg, out22);
  fuzzy->addFuzzyRule(FuzzyRule22);

  //23
  FuzzyRuleAntecedent *hipostdbavg = new FuzzyRuleAntecedent();
  hipostdbavg->joinWithAND(hipostd, belowavg);

  //23
  FuzzyRuleConsequent *out23 = new FuzzyRuleConsequent();
  out23->addOutput(tidak_optimal);

  //rule 23
  FuzzyRule *FuzzyRule23 = new FuzzyRule(23, hipostdbavg, out23);
  fuzzy->addFuzzyRule(FuzzyRule23);

  //24
  FuzzyRuleAntecedent *hipostdless = new FuzzyRuleAntecedent();
  hipostdless->joinWithAND(hipostd, less);

  //24
  FuzzyRuleConsequent *out24 = new FuzzyRuleConsequent();
  out24->addOutput(tidak_optimal);

  //rule 24
  FuzzyRule *FuzzyRule24 = new FuzzyRule(24, hipostdless, out24);
  fuzzy->addFuzzyRule(FuzzyRule24);

  //Hipotermia Bradycardia
  FuzzyRuleAntecedent *hipobrady = new FuzzyRuleAntecedent();
  hipobrady->joinWithAND(hipotermia, bradycardia);

  //25
  FuzzyRuleAntecedent *hipobradyavg = new FuzzyRuleAntecedent();
  hipobradyavg->joinWithAND(hipobrady, average);

  //25
  FuzzyRuleConsequent *out25 = new FuzzyRuleConsequent();
  out25->addOutput(tidak_optimal);

  //rule 25
  FuzzyRule *FuzzyRule25 = new FuzzyRule(25, hipobradyavg, out25);
  fuzzy->addFuzzyRule(FuzzyRule25);

  //26
  FuzzyRuleAntecedent *hipobradybavg = new FuzzyRuleAntecedent();
  hipobradybavg->joinWithAND(hipobrady, belowavg);

  //26
  FuzzyRuleConsequent *out26 = new FuzzyRuleConsequent();
  out26->addOutput(tidak_optimal);

  //rule 26
  FuzzyRule *FuzzyRule26 = new FuzzyRule(26, hipobradybavg, out26);
  fuzzy->addFuzzyRule(FuzzyRule26);

  //27
  FuzzyRuleAntecedent *hipobradyless = new FuzzyRuleAntecedent();
  hipobradyless->joinWithAND(hipobrady, less);

  //27
  FuzzyRuleConsequent *out27 = new FuzzyRuleConsequent();
  out27->addOutput(sangat_tidak_optimal);

  //rule 27
  FuzzyRule *FuzzyRule27 = new FuzzyRule(27, hipobradyless, out27);
  fuzzy->addFuzzyRule(FuzzyRule27);
}

String fuzzy_calibrate(float input1, float input2, float input3)
{
  fuzzy->setInput(1, input1);
  fuzzy->setInput(2, input2);
  fuzzy->setInput(3, input3);

  fuzzy->fuzzify();

  float output_kondisi = fuzzy->defuzzify(1);

  float val_sangat_tidak_opt = sangat_tidak_optimal->getPertinence();
  float val_tidak_opt = tidak_optimal->getPertinence();
  float val_optimal = optimal->getPertinence();
  Serial.print("sangat tidak optimal = ");
  Serial.println(val_sangat_tidak_opt);
  Serial.print("tidak optimal = ");
  Serial.println(val_tidak_opt);
  Serial.print("optimal = ");
  Serial.println(val_optimal);
  Serial.println();

  float value =  max(max(val_optimal, val_sangat_tidak_opt), val_tidak_opt);
  Serial.println(value);

  if (val_sangat_tidak_opt == value) {
    ;
    return "Sangat Tidak Optimal";
  }

  if (val_tidak_opt == value) {
    return "Tidak Optimal";
  }

  if (val_optimal == value) {
    return "Optimal";
  }
}

void setup() {
  Serial.begin(115200);
  fuzzy_begin();
}

void loop() {
  input1 = random(44, 45);
  input2 = random(90, 100);
  input3 = random(95, 100);
  fuzzy_calibrate(input1, input2, input3);
  delay(2000);
}