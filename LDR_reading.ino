#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <UrlEncode.h>

const char* ssid = "tambahan";
const char* password = "tambahan";

// +international_country_code + phone number
// Portugal +351, example: +351912345678
String phoneNumber = "+6281314742151";
//String apiKey = "77528412";
String apiKey = "4850095";

void sendMessage(String message) {

  // Data to send with HTTP POST
  //  String url = "https://api.whatabot.net/whatsapp/sendMessage?apikey=" + apiKey + "&text=" + urlEncode(message) + "&phone=" + phoneNumber;
  String url = "http://api.callmebot.com/whatsapp.php?phone=" + phoneNumber + "&apikey=" + apiKey + "&text=" + urlEncode(message);
  WiFiClient client;
  HTTPClient http;
  http.begin(client, url);

  // Specify content-type header
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  // Send HTTP POST request
  int httpResponseCode = http.POST(url);
  if (httpResponseCode == 200) {
    Serial.print("Message sent successfully");
  }
  else {
    Serial.println("Error sending the message");
    Serial.print("HTTP response code: ");
    Serial.println(httpResponseCode);
  }

  // Free resources
  http.end();
}

void setup() {
  Serial.begin(115200);
  pinMode(2, INPUT_PULLUP);
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

  // Send Message to WhatsAPP
//  sendMessage("Gasbocor!!");
}

void loop() {
  int sensorVal = digitalRead(2);
  //print out the value of the pushbutton
  Serial.println(sensorVal);
  if (sensorVal == HIGH) {
    //    digitalWrite(13, LOW);
    sendMessage("Gasbocor!!");
    delay(1000);
  } else {
//    digitalWrite(13, HIGH);
  }
}