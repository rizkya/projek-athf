#include <TinyGPSPlus.h> //import liblary TinyGPSPlus
#include <SoftwareSerial.h> //import liblary SoftwareSerial untuk komunikasi serial khusus esp
#include <WiFi.h> //import liblary WiFi 
#include <WiFiClient.h>//import liblary WiFiClient
#include <BlynkSimpleEsp32.h> //import liblary BlynkSimpleEsp32 agar dapat komunikasi dengan server Blynk
#include <Wire.h> ////import liblary Wire untuk komunikasi dengan sensor
#include <Adafruit_INA219.h> //import liblary Adafruit_INA219 untuk dapat mempermudah menggunakan sensor INA219
Adafruit_INA219 ina219; //inisialisasi nama sensor INA219
int lux1 = 0;
int outputValue = 0;
const int ldrpin = 32;  // pin input ldr
int LEDpin = 13; //pin output led
unsigned int move_index = 1;
int modde = 0; //
int switchhbuzzer = 0;
const int freq = 5000;
const int LEDchannel = 0;
const int resolution = 8;
int Buzzer = 27;
static const int RXPin = 16, TXPin = 17; //inisialisasi pin tx dan rx module gps
static const uint32_t GPSBaud = 9600; //inisialisasi baudrate komunikasi dengan module gps
TinyGPSPlus gps; //inisialisasi nama gps
WidgetMap myMap(V0); //inisialisasi WidgetMap pada interface Blynk

SoftwareSerial ss(RXPin, TXPin); //inisialisasi SoftwareSerial pada pin tx dan rx

#define BLYNK_PRINT Serial
char auth[] = "ezzy7Lp8oE0ii9-RkVk8pA2JGUaEFfDl"; //kode authentifikasi blynk agar dapat mengirim data pada blynk
char ssid[] = "Galaxy A51"; //nama WiFi
char pass[] = "12345678"; //password WiFi
//WidgetRTC rtc;
//=========================program inisialisasi=================================//
void setup() //fungsi setup untuk 1x run
{
  Serial.begin(115200); //memulai komunikasi serial dengan baudrate 115200
  ledcSetup(LEDchannel, freq, resolution); //mengatur pin led agar dapat diatur
  ledcAttachPin(LEDpin, LEDchannel); //menggatur pin led
  uint32_t currentFrequency;
  //  Blynk.begin(auth, ssid, pass);
  pinMode(Buzzer, OUTPUT);
  myMap.clear();
  ss.begin(GPSBaud); //memulai komunikasi dengan sensor gps dengan baudrate yang telah diatur
  Blynk.begin(auth, ssid, pass,  "iot.serangkota.go.id", 8080);
  if (! ina219.begin()) { //ketika sensor ina219 mengalami trouble
    Serial.println("Failed to find INA219 chip");
    while (1) {
      delay(10);
    }
  }
}
//============================================program void setup========================//

BLYNK_WRITE(V15)
{ int ini_nilai = param.asInt();
  if (ini_nilai == 1) {
    switchhbuzzer = 1;
  }
  else {
    switchhbuzzer = 0;
  }
}

BLYNK_WRITE(V5)
{
  int nilai = param.asInt(); // mengambil value dari server blynk
  if (nilai == 1) {
    modde = 0;
    Serial.println("modde");
    float shuntvoltage1 = 0;
    float busvoltage1 = 0;
    float current_mA1 = 0;
    float ledvoltage1 = 0;
    float led_power_mW1 = 0;
    shuntvoltage1 = ina219.getShuntVoltage_mV();
    busvoltage1 = ina219.getBusVoltage_V();
    current_mA1 = ina219.getCurrent_mA();
    led_power_mW1 = ina219.getPower_mW();
    ledvoltage1 = busvoltage1 + (shuntvoltage1 / 1000);
  }
  else {
    modde = 1;
  }
}
//============================================program mode automatic atau manual========================//
//=============================================Mode 1= Automatic / Mode 0= Manual======================//
BLYNK_WRITE(V1) // untuk mengatur mode alat (automatic/manual)
{
  int lampuu = param.asInt(); // Get value as integer
  if (modde == 1) { //ketika mode 1 maka kita dapat mengatur kecerahan lampu
    int lampuuu = map(lampuu, 0, 100, 0, 255); //mengubah value variable lampuuu
    ledcWrite(LEDchannel, lampuuu);
    float shuntvoltage3 = 0;
    float busvoltage3 = 0;
    float current_mA3 = 0;
    float ledvoltage3 = 0;
    float led_power_mW3 = 0;
    shuntvoltage3 = ina219.getShuntVoltage_mV();
    busvoltage3 = ina219.getBusVoltage_V();
    current_mA3 = ina219.getCurrent_mA();
    led_power_mW3 = ina219.getPower_mW();
    ledvoltage3 = busvoltage3 + (shuntvoltage3 / 1000);
  }
}
//============================================program lampu manual via blynk========================//
void displayInfo() //perintah untuk mengambil data gps dan waktu, lalu mengirimkannya pada server blynk
{
  Serial.print(F("Location: "));
  if (gps.location.isValid())
  { float latitude = (gps.location.lat());     //Storing the Lat. and Lon.
    float longitude = (gps.location.lng());
    Serial.print("latitude = ");
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(" , "));
    Serial.print("longitude = ");
    Serial.print(gps.location.lng(), 6);
    Blynk.virtualWrite(V8, String(latitude, 6));
    Blynk.virtualWrite(V7, String(longitude, 6));
    myMap.location(2, gps.location.lat(), gps.location.lng(), "Lokasi Lampu");
  }
  else
  {
    Serial.print(F("INVALID")); //ketika module gps tidak mendapatkan lokasi dari satelite
  }
  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid()) //gps mengambil data waktu dari satelite
  { int bulan = gps.date.month();
    int hari = gps.date.day();
    int tahun = gps.date.year();
    String tanggal = String(hari) + "/" + bulan + "/" + tahun; //mengubah data integer tanggal menjadi data string
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
    Blynk.virtualWrite(V4, tanggal);
  }
  else
  {
    Serial.print(F("INVALID"));
  }
  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    int jam = gps.time.hour() + 7;
    int menit = gps.time.minute();
    int detik = gps.time.second();
    String waktusekarang = String(jam) + ":" + menit + ":" + detik;  //mengubah data integer waktu menjadi data string
    if (gps.time.hour() < 10)
      Serial.print(jam);
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
    Serial.println("  Jam = ");
    Serial.println(waktusekarang);
    Blynk.virtualWrite(V2, waktusekarang);
  }
  else
  {
    Serial.print(F("INVALID"));
  }
}
//============================================program data gps dan juga waktu========================//


void daya() { //mengambil nilai arus beban, tegangan masuk, tegangan shunt, dan juga daya
  float shuntvoltage = 0;
  float busvoltage = 0;
  float current_mA = 0;
  float ledvoltage = 0;
  float led_power_mW = 0;
  shuntvoltage = ina219.getShuntVoltage_mV();
  busvoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  led_power_mW = ina219.getPower_mW();
  ledvoltage = busvoltage + (shuntvoltage / 1000);
  Serial.print("Bus Voltage:   "); Serial.print(busvoltage); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage); Serial.println(" mV");
  Serial.print("Led Voltage:  "); Serial.print(ledvoltage); Serial.println(" V");
  Serial.print("Current led:       "); Serial.print(current_mA); Serial.println(" mA");
  Serial.print("Power:         "); Serial.print(led_power_mW); Serial.println(" mW");
  Blynk.virtualWrite(V3, ledvoltage); //mengirimkan nilai voltage ke server blynk
  Blynk.virtualWrite(V6, current_mA); //mengirimkan nilai arus ke server blynk
  if (modde == 1 || modde == 0) {
    if ( led_power_mW < 100 && switchhbuzzer == 1) {
      Blynk.notify("Lampu Error!! Silahkan cek lampu!!"); //notifikasi popup pada android
      digitalWrite(Buzzer, HIGH); //buzzer aktif
      delay(1000);
      digitalWrite(Buzzer, LOW); //buzzer mati
    }
  }
  delay(500);
}
//============================================program pengambilan data voltage, arus dan daya========================//
void ukur() {  // mengambil data dari sensor ldr
  if (modde == 0) { //ketika mode 0 maka kecerahan lampu akan mengikuti input data dari sensor
    int LDRraw1 = analogRead(ldrpin);
    if (LDRraw1 >= 1200) {
      LDRraw1 = 1200;
    }
    Serial.print("Nilai LDR = ");
    Serial.println(LDRraw1);
    outputValue = map(LDRraw1, 0, 1200, 0, 255);
    Serial.println("===============================");
    ledcWrite(LEDchannel, outputValue); //output pwm sesuai dengan nilai lux yang sudah di conversi
    Serial.print("lampu = ");
    Serial.println(outputValue);
    float current_mA2 = 0;
    float led_power_mW2 = 0;
    current_mA2 = ina219.getCurrent_mA();
    led_power_mW2 = ina219.getPower_mW();
    delay(100);
  }
}
//============================================program pengambilan data dari ldr kalau mode automatic========================//

void loop()
{
  Blynk.run(); //memulai program blynk
  while (ss.available() > 0) //ketika komunikasi serial dengan module gps
    if (gps.encode(ss.read()))
      displayInfo(); //memanggil fungsi displayInfo
  daya(); //memanggil fungsi daya
  ukur(); //memanggil fungsi ukur
  if (millis() > 5000 && gps.charsProcessed() < 10) //ketika komunikasi dengan module gps tidak terjadi
  {
    Serial.println(F("No GPS detected: check wiring."));
    while (true);
  }
}

//============================================program eksekusi========================//
