#include <TinyGPSPlus.h> //import liblary TinyGPSPlus
#include <SoftwareSerial.h> //import liblary SoftwareSerial untuk komunikasi serial khusus esp
#include <WiFi.h> //import liblary WiFi 
#include <WiFiClient.h>//import liblary WiFiClient
#include <BlynkSimpleEsp32.h> //import liblary BlynkSimpleEsp32 agar dapat komunikasi dengan server Blynk
#include <Wire.h> ////import liblary Wire untuk komunikasi dengan sensor
#include <Adafruit_INA219.h> //import liblary Adafruit_INA219 untuk dapat mempermudah menggunakan sensor INA219
Adafruit_INA219 ina219; //inisialisasi nama sensor INA219
int lux1 = 0;
int LEDpin = 13;
unsigned int move_index = 1;
int modde = 0;
const int freq = 5000;
const int LEDchannel = 0;
const int resolution = 8;
static const int RXPin = 16, TXPin = 17; //inisialisasi pin tx dan rx module gps
static const uint32_t GPSBaud = 9600; //inisialisasi baudrate komunikasi dengan module gps
TinyGPSPlus gps; //inisialisasi nama gps
WidgetMap myMap(V0); //inisialisasi WidgetMap pada interface Blynk

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin); //inisialisasi SoftwareSerial pada pin tx dan rx

#define BLYNK_PRINT Serial

/* Fill-in your Template ID (only if using Blynk.Cloud) */
//#define BLYNK_TEMPLATE_ID   "YourTemplateID"




// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "ezzy7Lp8oE0ii9-RkVk8pA2JGUaEFfDl"; //kode authentifikasi blynk agar dapat mengirim data pada blynk

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "tambahan"; //nama WiFi
char pass[] = "depok212"; //password WiFi

void setup()
{
  Serial.begin(115200); //memulai komunikasi serial dengan baudrate 115200
  ledcSetup(LEDchannel, freq, resolution); //mengatur pin led agar dapat diatur
  ledcAttachPin(LEDpin, LEDchannel); //menggatur pin led
  uint32_t currentFrequency;
  //  Blynk.begin(auth, ssid, pass);
  myMap.clear();
  ss.begin(GPSBaud); //memulai komunikasi dengan sensor gps dengan baudrate yang telah diatur
  Blynk.begin(auth, ssid, pass,  "iot.serangkota.go.id", 8080);
  if (! ina219.begin()) { //ketika sensor ina219 mengalami trouble
    Serial.println("Failed to find INA219 chip");
    while (1) {
      delay(10);
    }
  }
}

BLYNK_WRITE(V5)
{
  int nilai = param.asInt(); // mengambil value dari server blynk

  if (nilai == 1) {
    modde = 0;
    Serial.println("modde");
    float shuntvoltage1 = 0;
    float busvoltage1 = 0;
    float current_mA1 = 0;
    float ledvoltage1 = 0;
    float led_power_mW1 = 0;
    shuntvoltage1 = ina219.getShuntVoltage_mV();
    busvoltage1 = ina219.getBusVoltage_V();
    current_mA1 = ina219.getCurrent_mA();
    led_power_mW1 = ina219.getPower_mW();
    ledvoltage1 = busvoltage1 + (shuntvoltage1 / 1000);

    if (modde = 0 && current_mA1 == 0 && gps.location.isValid()) {
      float latitude1 = (gps.location.lat());     //Storing the Lat. and Lon.
      float longitude1 = (gps.location.lng());
      char pesan[60];
      sprintf(pesan, "lokasi lampu rusak %d latitude %s longitude", latitude1, longitude1); //isi dari email
      Serial.println(pesan);
      Blynk.email("bellcranel008@gmail.com", "Lampu rusak", pesan); //mengirimkan pesan pada email
      Blynk.notify(pesan); //notifikasi popup pada android

    }
  }
  else {
    modde = 1;
  }
}

BLYNK_WRITE(V1)
{
  int lampuu = param.asInt(); // Get value as integer
  if (modde == 1) { //ketika mode 1 maka kita dapat mengatur kecerahan lampu

    int lampuuu = map(lampuu, 0, 100, 0, 255); //mengubah value variable lampuuu
    ledcWrite(LEDchannel, lampuuu);
  }
}

void displayInfo() //perintah untuk mengambil data gps dan mengirimkannya pada server blynk
{
  Serial.print(F("Location: "));
  if (gps.location.isValid())
  { float latitude = (gps.location.lat());     //Storing the Lat. and Lon.
    float longitude = (gps.location.lng());
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
    Blynk.virtualWrite(V8, String(latitude, 6));
    Blynk.virtualWrite(V7, String(longitude, 6));
    myMap.location(2, gps.location.lat(), gps.location.lng(), "Lokasi Lampu");

  }
  else
  {
    Serial.print(F("INVALID")); //ketika module gps tidak mendapatkan lokasi dari satelite
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid()) //gps mengambil data waktu dari satelite
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
}

void daya() { //mengambil nilai arus beban, tegangan masuk, tegangan shunt, dan juga daya
  float shuntvoltage = 0;
  float busvoltage = 0;
  float current_mA = 0;
  float ledvoltage = 0;
  float led_power_mW = 0;
  shuntvoltage = ina219.getShuntVoltage_mV();
  busvoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  led_power_mW = ina219.getPower_mW();
  ledvoltage = busvoltage + (shuntvoltage / 1000);
  Serial.print("Bus Voltage:   "); Serial.print(busvoltage); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage); Serial.println(" mV");
  Serial.print("Led Voltage:  "); Serial.print(ledvoltage); Serial.println(" V");
  Serial.print("Current led:       "); Serial.print(current_mA); Serial.println(" mA");
  Serial.print("Power:         "); Serial.print(led_power_mW); Serial.println(" mW");
  Blynk.virtualWrite(V3, ledvoltage); //mengirimkan nilai voltage ke server blynk
  Blynk.virtualWrite(V6, current_mA); //mengirimkan nilai arus ke server blynk
  delay(500);
}

void ukur() {
  if (modde == 0) { //ketika mode 0 maka kecerahan lampu akan mengikuti input data dari sensor
    //Multimapping LDR Luar
    int out1[] = {0, 20, 25, 30, 100, 200, 300, 500, 700, 1070}; //lux1
    int in1[] = {1600, 2030, 2190, 2320, 2350, 2600, 2635, 2665, 2700, 2900}; //ADC

    delay(1000);
    int LDRraw1 = analogRead(25);
    float lux1 = multiMap(LDRraw1, in1, out1, 10);
    if (lux1 < 1074) {
      lux1 = lux1;
    }
    else if (lux1 > 20000) {
      lux1 = lux1;
    }
    else {
      lux1 = lux1 - 1074;
    }

    Serial.print("ADC: ");
    Serial.println(LDRraw1);
    Serial.print("lux1: ");
    Serial.println(lux1);
    Serial.println("===============================");
    float alog = map(lux1, 0, 1070, 255, 0); //mengubah nilai lux menjadi nilai pwm
    ledcWrite(LEDchannel, alog); //output pwm sesuai dengan nilai lux yang sudah di conversi
    Serial.print("lampu = ");
    Serial.println(alog);
    Blynk.virtualWrite(V2, lux1); //mengirimkan data nilai lux ke server blynk
    //
    float current_mA2 = 0;
    current_mA2 = ina219.getCurrent_mA();
    if (modde = 1 && current_mA2 == 0 && gps.location.isValid()) {
      float latitude = (gps.location.lat());     //Storing the Lat. and Lon.
      float longitude = (gps.location.lng());
      char pesan1[60];
      sprintf(pesan1, "lokasi lampu rusak %d latitude %s longitude", latitude, longitude); //isi dari email
      Serial.println(pesan1);
      Blynk.email("bellcranel008@gmail.com", "Lampu rusak", pesan1); //mengirimkan pesan pada email
      Blynk.notify(pesan1); //notifikasi popup pada android
      delay(500);
    }

  }
}



int multiMap(int val, int* _in, int* _out, uint8_t size) //konversi nilai analog ke nilai lux
{
  // take care the value is within range
  // val = constrain(val, _in[0], _in[size-1]);
  if (val <= _in[0]) return _out[0];
  if (val >= _in[size - 1]) return _out[size - 1];

  // search right interval
  uint8_t pos = 1;  // _in[0] allready tested
  while (val > _in[pos]) pos++;

  // this will handle all exact "points" in the _in array
  if (val == _in[pos]) return _out[pos];

  // interpolate in the right segment for the rest
  return (val - _in[pos - 1]) * (_out[pos] - _out[pos - 1]) / (_in[pos] - _in[pos - 1]) + _out[pos - 1];
}

void loop()
{
  Blynk.run(); //memulai program blynk
  while (ss.available() > 0) //ketika komunikasi serial dengan module gps
    if (gps.encode(ss.read()))
      displayInfo(); //memanggil fungsi displayInfo
  daya(); //memanggil fungsi daya
  ukur(); //memanggil fungsi ukur
  if (millis() > 5000 && gps.charsProcessed() < 10) //ketika komunikasi dengan module gps tidak terjadi
  {
    Serial.println(F("No GPS detected: check wiring."));
    while (true);
  }

}
